# Terminals Working Group Specifications

This repository contains specifications for terminal emulator protocols under the purview of the Terminals Working Group.

## Proposals

To create a new proposal:

1. Clone the repo.
2. Write your draft proposal using the template in `proposals/template.md`. Place the new file in the `proposals` directory.
3. Send a merge request to merge it into this repo.
4. The propsal will be discussed, modified if needed, and finally merged or moved into the `rejected` directory.

An accepted proposal is not a standard, but is on the track to becoming a standard.

## Acceptance

After a proposal is accepted, create a new merge request to move it into `accepted`. Once it is ready the PR will be merged.
